This lists all the class names of elements on the Learning Page:

1. Header
    * Header: `header`
    * Logo Text: `brand-text`

2. Body
    * Body Background: `module-page`

3. Progress Bar
    * Progress Bar Background: `progressbar-container`
    * Progress Bar Container: `max-progress`
    * Progress Bar Progress: `current-progress`

4. Subtopics
    * Background: `sidebar, mob-sidebar`
    * Chapter Title Font: `chapter-title`
    * Subtopics Generally: `subtopics`
    * Accessible Subtopics: `subtopics__accessible`
    * Inaccessible Subtopics: `subtopics__inaccessible`
    * Current Subtopic Indicator Colour: `current-indicator`

5. Content Container
    * Background: `content-container`


6. Question/Description
    * Background: `question-card`
    * Description: `desc-text`
    * Question: `question-text`


7. Media
    * Border: `animation-container`


8. Question Choices
    * Background: `data-choice`
    * Correct Answer Background: `correct-answer-theme`
    * Wrong Answer Background: `wrong-answer-theme`
    * Got It! Button Background: `feedback-cta`
